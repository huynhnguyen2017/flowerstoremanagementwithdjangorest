from django.urls import path
from providermanagement import views

urlpatterns = [
    path('one/create/', views.NewProvider.as_view(), name='provider-one-create'),
    path('list/', views.ProviderList.as_view(), name='provider-list'),
]
import jwt
from django.db import IntegrityError
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist

from stockmanagement.ProjectViews.utils.variable_config import JWT_SECRET, JWT_ALGORITHM
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken, CustomerIsAuthenticatedOrRefreshToken
from stockmanagement.ProjectViews.utils.decodeToken import decode_and_get_user_id
from providermanagement.serializers import ProviderSerializer
from stockmanagement.models import Providers
from usermanagement.models import Users


class NewProvider(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            serializer = ProviderSerializer(request.data)
            try:
                p_name = request.data['provider_name'] or None
                p_address = request.data['address'] or None
                p_phone = request.data['phone'] or None
                p_representative = request.data['representative'] or None
                Providers.objects.create(provider_name=p_name, address=p_address, phone='p_phone',
                                         representative=p_representative)

                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except MultipleObjectsReturned:
                return Response({'code': 400,
                                 'message': 'provider existed'}, status=status.HTTP_400_BAD_REQUEST)
            except IntegrityError:
                return Response({'code': 400,
                                 'message': 'provider existed'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class ProviderList(APIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        if api_token:
            try:
                # get user id from token
                payload = jwt.decode(api_token, JWT_SECRET,
                                     algorithms=[JWT_ALGORITHM])
                user = Users.objects.get(id=payload['user_id'])
                providers = Providers.objects.all()
                serializer = ProviderSerializer(providers, many=True)
                # print(serializer.data)
                return Response(serializer.data, status=status.HTTP_200_OK)

            except (jwt.ExpiredSignatureError, jwt.DecodeError, ObjectDoesNotExist):
                return Response({'code': 400,
                                 'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)

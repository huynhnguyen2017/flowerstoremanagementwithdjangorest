from django.db import models


# main object
class Providers(models.Model):
    provider_name = models.CharField(max_length=45, blank=True, default='')
    address = models.CharField(max_length=100, blank=True, default='')
    phone = models.CharField(max_length=17, blank=True, default='')
    representative = models.CharField(max_length=45, blank=True, default='')
    is_existed = models.BooleanField(default=True)

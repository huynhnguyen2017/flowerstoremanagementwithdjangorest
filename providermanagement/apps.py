from django.apps import AppConfig


class ProvidermanagementConfig(AppConfig):
    name = 'providermanagement'

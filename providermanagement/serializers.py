from rest_framework import serializers
from .models import Providers


# provider instance
class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Providers
        fields = ['id', 'provider_name', 'address', 'phone', 'representative', 'is_existed']
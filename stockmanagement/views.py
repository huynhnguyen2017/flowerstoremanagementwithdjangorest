import pytz
from rest_framework.views import APIView

from flowerexchange.models import Invoice
from importmanagement.models import Import
from importmanagement.serializers import ImportSerializer

from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from django.utils import timezone
from datetime import date, datetime

from stockmanagement.ProjectViews.utils.decodeToken import decode_and_get_user_id
from stockmanagement.models import Users, Stock
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from stockmanagement.serializers import StockSerializer
from usermanagement.serializers import UserSerializer

# fix
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()
# from django.shortcuts import get_object_or_404
# from django.conf import settings
# from django.contrib.auth import get_user_model as user_model
# User = user_model()




class MailVerification(generics.RetrieveAPIView):

    def get(self, request, *args, **kwargs):
        try:
            token = kwargs['token']
            user_id = decode_and_get_user_id(token)

            # check user id here
            user_ = Users.objects.get(id=user_id)
            serializer = UserSerializer(user_, data={'is_active': True}, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'code': 200,
                                 'message': 'verify successfully'}, status=status.HTTP_200_OK)
            return Response({'code': 400,
                             'message': 'Fail to verify'}, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)


class StockListByInterval(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        start_time = request.data['startTime']
        end_time = request.data['endTime']
        stocks_ = Stock.objects.filter(stock_time__range=(start_time, end_time))

        serializer = StockSerializer(stocks_, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


# store stock with condition one import per day
class StockStore(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token) or None
        old_number = 0
        try:
            timezone.activate(pytz.timezone('Asia/Ho_Chi_Minh'))
            start_time = timezone.now().replace(hour=0, minute=0)
            end_time = timezone.now().replace(hour=timezone.now().hour + 7)
            print('start time: ', start_time)
            print('end time: ', end_time)
            imports_ = Import.objects.filter(import_date__range=(start_time, end_time), is_existed=True)

            print('import: ', imports_)
            # stock date time
            stock_start_time = timezone.now().replace(day=date.today().day - 1)

            # get stock based on time (from previous day to today)
            stock_ = Stock.objects.filter(stock_time__range=(stock_start_time, end_time)).order_by('-stock_time')[:1]
            imports_number = imports_.aggregate(Sum('import_quantity'))
            # user = Users.objects.get(id=user_id)
            user = User.objects.get(id=user_id)

            if stock_:
                stock_serializer = StockSerializer(stock_, many=True)  # remember to add may=True
                # print(stock_serializer.data[0]['stock_time'])
                pre_import_benchmark = stock_serializer.data[0]['stock_time']
                # get invoices from the previous import to identify how many flowers sold

                invoices_ = Invoice.objects.filter(invoice_time__gte=pre_import_benchmark, paid=True) \
                    .annotate(quantity_total=Sum('quantity'))

                if len(invoices_) > 0:
                    # get quantity of flowers sold
                    sale_total = invoices_[0].quantity_total
                else:
                    sale_total = 0

                # get old number of flower in stock
                old_number = stock_serializer.data[0]['stock_newquantity'] - sale_total
                if old_number < 0:
                    old_number = 0
                print('if statement')
                Stock.objects.create(stock_oldquantity=old_number,
                                     stock_newquantity=imports_number['import_quantity__sum'],
                                     Import_id=imports_[0], Users_id=user)

            else:
                print('else statement')
                Stock.objects.create(stock_oldquantity=0,
                                     stock_newquantity=imports_number['import_quantity__sum'],
                                     Import_id=imports_[0], Users_id=user)

            # update is_existed field to mark the import instance was stored in stock
            success_imports = []
            for obj in imports_:
                print('new one: ', obj)
                serializer = ImportSerializer(obj, data={'is_existed': False}, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    success_imports.append(serializer.data)

            return Response({'code': 200,
                             'oldNumber': old_number,
                             'newQuantity': imports_number['import_quantity__sum']}, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)
        # when no new import found
        except IndexError:
            return Response({'code': 400,
                             'message': 'new import not found'}, status=status.HTTP_400_BAD_REQUEST)

from rest_framework import serializers

from flowermanagement.models import FlowerPrice, FlowerCategories
from stockmanagement.models import Stock


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ['id', 'stock_oldquantity', 'stock_newquantity',
                  'stock_time', 'is_existed', 'Import_id_id']

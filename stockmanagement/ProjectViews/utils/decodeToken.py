import jwt

from .variable_config import JWT_ALGORITHM, JWT_SECRET


def decode_and_get_user_id(token):
    api_token = token
    if api_token:
        try:
            # decode token from request
            payload = jwt.decode(api_token, JWT_SECRET, algorithms=[JWT_ALGORITHM])

            return payload['user_id']

        # raises jwt.ExpiredSignatureError if the expiration time is in the past or
        # no successful decoding
        except (jwt.ExpiredSignatureError, jwt.DecodeError):
            return False
    return False

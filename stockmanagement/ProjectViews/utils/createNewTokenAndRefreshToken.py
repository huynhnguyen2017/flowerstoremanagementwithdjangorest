from datetime import datetime, timedelta
import jwt
from .variable_config import TOKEN_EXP_SECONDS, REFRESH_TOKEN_EXP_SECONDS, JWT_SECRET, JWT_ALGORITHM


def create_token_and_refresh_token(user_id):
    token_payload = {
        'user_id': user_id,
        'exp': datetime.utcnow() + timedelta(seconds=TOKEN_EXP_SECONDS)
    }

    refreshToken_payload = {
        'user_id': user_id,
        'exp': datetime.utcnow() + timedelta(seconds=REFRESH_TOKEN_EXP_SECONDS)
    }

    # encode payload
    token = jwt.encode(token_payload, JWT_SECRET, JWT_ALGORITHM)
    refresh_token = jwt.encode(refreshToken_payload, JWT_SECRET, JWT_ALGORITHM)

    return token, refresh_token

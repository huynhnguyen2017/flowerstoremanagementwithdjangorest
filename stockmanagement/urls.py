from django.urls import path
from stockmanagement import views

urlpatterns = [
    path('sendgrid/<str:token>/', views.MailVerification.as_view(), name='sendgrid'),
    path('stock/store/', views.StockStore.as_view(), name='stock-store'),
    path('stock/interval/list/', views.StockListByInterval.as_view(), name='stock-interval-list'),
]

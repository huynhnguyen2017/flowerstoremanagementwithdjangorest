from rest_framework import permissions, exceptions
import jwt

from stockmanagement.models import Users

JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
PERMISSION_RESPONSE = 'You are granting for performing this task'


class OwnerIsAuthenticatedOrRefreshToken(permissions.BasePermission):

    def has_permission(self, request, view):
        api_token = request.META.get('HTTP_AUTHORIZATION')

        if api_token:
            try:
                payload = jwt.decode(api_token, JWT_SECRET,
                                     algorithms=[JWT_ALGORITHM])

                user = Users.objects.get(id=payload['user_id'])

                if user.is_staff or user.is_superuser:
                    return True

                raise exceptions.PermissionDenied(detail=PERMISSION_RESPONSE)

            except (jwt.ExpiredSignatureError, jwt.DecodeError, Users.DoesNotExist):
                raise exceptions.PermissionDenied(detail='invalid authentication')

        raise exceptions.NotAuthenticated(detail='invalid authentication')


class CustomerIsAuthenticatedOrRefreshToken(permissions.BasePermission):

    def has_permission(self, request, view):
        api_token = request.META.get('HTTP_AUTHORIZATION')

        if api_token:
            try:
                payload = jwt.decode(api_token, JWT_SECRET, algorithms=JWT_ALGORITHM)

                user = Users.objects.get(id=payload['user_id'])

                if user.is_active:
                    return True
                raise exceptions.PermissionDenied(detail=PERMISSION_RESPONSE)

            except (jwt.ExpiredSignatureError, jwt.DecodeError, Users.DoesNotExist):
                raise exceptions.PermissionDenied(detail='invalid authentication')

        raise exceptions.NotAuthenticated(detail='invalid authentication')


class AdminIsAuthenticatedOrRefreshToken(permissions.BasePermission):

    def has_permission(self, request, view):
        api_token = request.META.get('HTTP_AUTHORIZATION')

        if api_token:
            try:
                payload = jwt.decode(api_token, JWT_SECRET, algorithms=JWT_ALGORITHM)

                user = Users.objects.get(id=payload['user_id'])

                if user.role == 'admin':
                    return True
                raise exceptions.PermissionDenied(detail=PERMISSION_RESPONSE)

            except (jwt.ExpiredSignatureError, jwt.DecodeError, Users.DoesNotExist):
                raise exceptions.PermissionDenied(detail='invalid authentication')

        raise exceptions.NotAuthenticated(detail='invalid authentication')

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

from flowermanagement.models import Flowers
from importmanagement.models import Import
from providermanagement.models import Providers
from usermanagement.models import Users


class Stock(models.Model):
    stock_oldquantity = models.IntegerField(default=0)
    stock_newquantity = models.IntegerField(default=0)
    stock_time = models.DateTimeField(auto_now_add=True)
    is_existed = models.BooleanField(default=True)
    Import_id = models.ForeignKey(Import, on_delete=True, related_name='stock_import')
    Users_id = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=True, related_name='stock_users')

# profile
import hashlib
import os

from rest_framework.views import APIView
from datetime import datetime, timedelta
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import From, To, PlainTextContent, HtmlContent, Mail
from django.db.utils import IntegrityError

from rest_framework import generics

from stockmanagement.ProjectViews.utils.createNewTokenAndRefreshToken import create_token_and_refresh_token
from stockmanagement.ProjectViews.utils.decodeToken import decode_and_get_user_id
from stockmanagement.ProjectViews.utils.variable_config import SALT
from stockmanagement.permissions import CustomerIsAuthenticatedOrRefreshToken
import jwt
from usermanagement.models import Users
from usermanagement.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

# set variable - encode and decode with jwt authentication
JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
TOKEN_EXP_SECONDS = 60*10  # 10 minutes
REFRESH_TOKEN_EXP_SECONDS = 60*60*24  # 1 day


# register
class UserRegister(generics.CreateAPIView):
    def post(self, request, *args, **kwargs):
        try:
            email = request.data['email']

            password = request.data['password']

            re_password = request.data['repeatPassword']
            if password == re_password:
                # hash password with hashlib python package
                # it is tunable, slow and including a salt, and it spends massive time decrypting
                pwd = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), SALT, 100000)

                user = Users.objects.create(email=email, password=pwd.hex(), is_active=False,
                                            fullname='', birthday='2020-07-07', gender='',
                                            favorite_flower='', is_staff=True)

                user_id = Users.objects.get(email=email)

                token_payload = {
                    'user_id': user_id.id,
                    'exp': datetime.utcnow() + timedelta(seconds=TOKEN_EXP_SECONDS)
                }

                # encode payload
                token_link = jwt.encode(token_payload, JWT_SECRET, JWT_ALGORITHM)

                # set up SENDGRID_API_KEY with export SENDGRID_API_KEY= {{key???}} or in .env file
                # echo "export SENDGRID_API_KEY={{key??}}" > .env
                sendgrid_client = SendGridAPIClient(
                    api_key=os.environ.get('SENDGRID_API_KEY')
                )

                from_email = From('huynh.hoang.nhh@gmail.com')  # align with registerd email on sendgrid (sender)
                to_email = To(email)  # any mail
                subject = 'Xác thực tài khoản người dùng'
                plain_text_content = PlainTextContent('Chúng tôi rất hân hạnh được phục vụ bạn')
                html_content = HtmlContent(
                    '<div>Rất mong bạn sẽ có những trải nghiệm tuyệt vời <br /><a href=http://127.0.0.1:8000/sendgrid/'
                    + token_link.decode('utf-8') + '/ >Verify here</a></div>')

                message = Mail(from_email, to_email, subject, plain_text_content, html_content)
                response = sendgrid_client.send(message=message)

                return Response({'code': 201,
                                 'message': 'register successfully and verified email sent'},
                                status=status.HTTP_201_CREATED)
            return Response({'code': 400,
                             'message': 'repeat password invalid'}, status=status.HTTP_400_BAD_REQUEST)
        except MultipleObjectsReturned:
            return Response({'code': 400,
                             'message': 'user existed'}, status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError:
            return Response({'code': 400,
                             'message': 'user existed'}, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            return Response({'code': 400,
                             'message': 'miss fields'}, status=status.HTTP_400_BAD_REQUEST)


# update profile
class UserProfileUpdate(APIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def put(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            try:
                user = Users.objects.get(id=user_id)

                serializer = UserSerializer(user, data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    res = {
                        "fullname": serializer.data['fullname'],
                        "birthday": serializer.data['birthday'],
                        "gender": serializer.data['gender'],
                        "favorite_flower": serializer.data['favorite_flower'],
                    }

                    return Response(res, status=status.HTTP_200_OK)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except ObjectDoesNotExist:
                return Response({'code': 400,
                                 'message': 'user not found'}, status=status.HTTP_400_BAD_REQUEST)
            except MultipleObjectsReturned:
                return Response({'code': 400,
                                 'message': 'invalid user'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'code': 400,
                         'message': 'no user id'}, status=status.HTTP_400_BAD_REQUEST)


class UserProfile(generics.RetrieveAPIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def get(self, request, *args, **kwargs):
        try:
            api_token = request.META.get('HTTP_AUTHORIZATION')
            # decode payload to get user id
            payload = jwt.decode(api_token, JWT_SECRET,
                                 algorithms=[JWT_ALGORITHM])
            # get user information from user id extracted from database
            user_profile = Users.objects.get(id=payload['user_id'])

            # serialize data
            user_profile_serializer = UserSerializer(user_profile)
            # user profile data
            u_fullname = user_profile_serializer.data['fullname']
            u_birthday = user_profile_serializer.data['birthday']
            u_gender = user_profile_serializer.data['gender']
            u_favorite_flower = user_profile_serializer.data['favorite_flower']

            return Response({
                'code': 200,
                'fullname': u_fullname,
                'dateOfBirth': u_birthday,
                'gender': u_gender,
                'userFavorite': u_favorite_flower
            }, status=status.HTTP_200_OK)

        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return Response({'code': 400,
                             'message': 'token is expired'}, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'User Profile not exist'}, status=status.HTTP_400_BAD_REQUEST)
        except MultipleObjectsReturned:
            return Response({'code': 400,
                             'message': 'multiple users were found'}, status=status.HTTP_400_BAD_REQUEST)


# refresh token
class UserRefreshToken(APIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        # acquire encoded token from header request
        api_token = request.META.get('HTTP_AUTHORIZATION')
        if api_token:
            try:
                # decode token from request
                payload = jwt.decode(api_token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
                # create new pair of token and refresh token
                u_token, u_refresh_token = create_token_and_refresh_token(payload['user_id'])

                # look up existed user
                user = Users.objects.get(id=payload['user_id'])

                # save new token pair to database
                user.last_login = u_token.decode('utf-8')
                user.last_refresh = u_refresh_token.decode('utf-8')
                user.save()

                return Response({'code': 200,
                                 'token': u_token.decode('utf-8'),
                                 'refreshToken': u_refresh_token.decode('utf-8')}, status=status.HTTP_200_OK)
            # catch object not found in database or return multiple object
            except (ObjectDoesNotExist, MultipleObjectsReturned):
                return Response({'code': 400,
                                 'message': 'Invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)
            # raises jwt.ExpiredSignatureError if the expiration time is in the past or no successful decoding
            except (jwt.ExpiredSignatureError, jwt.DecodeError):
                return Response({'code': 400,
                                 'message': 'Invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'code': 400,
                        'message': 'Invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


# login
class UserLogin(generics.CreateAPIView):
    def post(self, request, *args, **kwargs):

        # remove space in data
        user = request.data['email']

        # hash password
        pwd = hashlib.pbkdf2_hmac('sha256', request.data['password'].encode('utf-8'), SALT, 100000)

        # raw check to SQL injection 1-- or --
        checked_user = '--' in user
        if checked_user:
            # print('check: ', checked_user)
            return Response({'code': 400, 'message': 'login is failed'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            # look up user based on username and password
            user = Users.objects.get(email=user, password=pwd.hex())  # check with hashed password

            print('user active: ', user.is_active)
            if user.is_active:
                # serialize object from model
                serializer = UserSerializer(instance=user)

                # get a pair of token and refresh token encoded
                u_token, u_refresh_token = create_token_and_refresh_token(user.id)

                # save token and refresh token to database
                user.last_login = u_token.decode('utf-8')
                user.last_refresh = u_refresh_token.decode('utf-8')
                user.save()

                return Response({'code': 200,
                                 'token': u_token.decode('utf-8'),
                                 'refreshToken': u_refresh_token.decode('utf-8')}, status=status.HTTP_200_OK)
            return Response({'message': 'login is failed'}, status=status.HTTP_400_BAD_REQUEST)
        # catch raise object not found in database from 'get' method
        except ObjectDoesNotExist:
            return Response({'message': 'login is failed'}, status=status.HTTP_400_BAD_REQUEST)
        # catch raise multiple objects are returned
        except MultipleObjectsReturned:
            return Response({'message': 'mulitple'}, status=status.HTTP_400_BAD_REQUEST)


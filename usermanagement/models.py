
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

from stockmanagement.managers import CustomUserManager


# main object
class Users(AbstractUser):
    username = None
    first_name = None
    last_name = None
    last_login = None
    is_active = models.BooleanField(default=False)
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    password = models.CharField(max_length=100, blank=False, null=False, default='')
    fullname = models.CharField(max_length=100, blank=True, default='')
    birthday = models.DateField(blank=True, default='2020-07-07')
    gender = models.CharField(max_length=8, blank=True, default='')
    favorite_flower = models.CharField(max_length=100, blank=True, default='')

    # def __str__(self):
    #     return self.email
    # def get_instance(self):
    #     return self.id

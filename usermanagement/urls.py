from django.urls import path
from usermanagement import views

urlpatterns = [
    path('profile/', views.UserProfile.as_view(), name='user-profile'),
    path('token/refresh/', views.UserRefreshToken.as_view(), name='user-refresh-token'),
    path('login/', views.UserLogin.as_view(), name='user-login'),
    path('profile/update/', views.UserProfileUpdate.as_view(), name='user-profile-update'),
    path('register/', views.UserRegister.as_view(), name='user-register'),
]

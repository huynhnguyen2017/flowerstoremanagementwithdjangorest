from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param
from rest_framework import pagination


# Custom pagination: https://medium.com/@zoejoyuliao/django-rest-framework-add-custom-pagination-c758a4f127fa
class CustomPagination(pagination.PageNumberPagination):
    page_size = 3
    page_query_param = 'page_number'
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response({
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'total': self.page.paginator.count,
            'data': data
        })


class MyLimitOffsetPagination(pagination.LimitOffsetPagination):
    default_limit = 20
    max_limit = 1000

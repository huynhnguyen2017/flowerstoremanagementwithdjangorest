from django.urls import path
from flowerexchange import views

urlpatterns = [
    path('interval/list/', views.InvoiceListByInterval.as_view(), name='invoice-interval-list'),
    path('one/create/', views.InvoiceNew.as_view(), name='invoice-one-create'),
    path('list/', views.InvoiceList.as_view(), name='invoice-list'),
    path('unpaid/customers/list/', views.UnpaidInvoiceCustomer.as_view(), name='unpaid-customers-list'),
    path('exchange/customer/list/', views.ExchangeListByCustomer.as_view(),
         name='invoice-exchange-customer-list'),
    path('one/exchange/customer/pay/', views.OneExchangePayByUser.as_view(), name='one-exchange-customer-pay'),
    path('multiple/exchange/customer/pay/', views.MultipleExchangePayByUser.as_view(),
         name='multiple-exchange-customer-pay'),
    path('statistics/interval/user/', views.TwentyFlowerByUser.as_view(), name='statistics-interval-user'),
    path('statistics/ten/flower/user/', views.TenFlowerMostPurchase.as_view(), name='statistics-ten-flower-user'),
    path('statistics/oneday/total/', views.FlowerSaleOneDay.as_view(), name='statistics-oneday-total'),
    path('statistics/oneday/customer/total', views.CustomerCountOneDay.as_view(),
         name='statistics-oneday-customer-total'),
    path('statistics/quantity/left/', views.LeftFlowerQuantity.as_view(), name='statistics-quantity-left'),
    path('benefit/one/day/', views.NetBenefitOverDay.as_view(), name='benefit-one-day'),
    path('benefit/interval/', views.NetBenefitByInterval.as_view(), name='benefit-interval'),
]

from django.apps import AppConfig


class FlowerexchangeConfig(AppConfig):
    name = 'flowerexchange'

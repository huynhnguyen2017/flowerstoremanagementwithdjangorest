from django.db import models
from stockmanagement.models import Flowers
from stockmanagement.models import Users
from stockmanagement.models import Stock


class Invoice(models.Model):
    quantity = models.IntegerField(default=0)
    invoice_cost = models.FloatField(default=0.0)
    exchange_method = models.CharField(max_length=45, blank=False, default='in-place')
    invoice_time = models.DateTimeField(auto_now_add=True)
    is_existed = models.BooleanField(default=True)
    paid = models.BooleanField(default=False)
    Flowers_id = models.ForeignKey(Flowers, on_delete=True, related_name='invoice_flowers')
    Users_id = models.ForeignKey(Users, on_delete=True, related_name='users_invoice')
    Stock_id = models.ForeignKey(Stock, on_delete=True, related_name='stock_invoice')
    receive_address = models.CharField(max_length=100, blank=False, default='no exist')

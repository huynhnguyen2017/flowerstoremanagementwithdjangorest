from rest_framework.response import Response
from rest_framework import status, generics
from django.core.exceptions import ObjectDoesNotExist
import datetime

from flowermanagement.models import FlowerPrice
from stockmanagement.ProjectViews.utils.decodeToken import decode_and_get_user_id
from stockmanagement.models import Flowers, Users, Stock
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken, CustomerIsAuthenticatedOrRefreshToken
from flowermanagement.serializers import FlowerSerializer, FlowerPriceSerializer
from usermanagement.serializers import UserSerializer
from .models import Invoice
from .serializers import InvoiceSerializer
from .paginations import MyLimitOffsetPagination, CustomPagination
# import other views
from flowerexchange.InvoiceViews.ExchangeViews.ExchangeListByCustomer import ExchangeListByCustomer
from flowerexchange.InvoiceViews.ExchangeViews.OneExchangePayByUser import OneExchangePayByUser
from flowerexchange.InvoiceViews.ExchangeViews.MultipleExchangePayByUser import MultipleExchangePayByUser
from flowerexchange.InvoiceViews.StatisticsViews.TwentyFlowerByUser import TwentyFlowerByUser
from flowerexchange.InvoiceViews.StatisticsViews.TenFlowerMostPurchase import TenFlowerMostPurchase
from flowerexchange.InvoiceViews.StatisticsViews.FlowerSaleOneDay import FlowerSaleOneDay
from flowerexchange.InvoiceViews.StatisticsViews.CustomerCountOneDay import CustomerCountOneDay
from flowerexchange.InvoiceViews.StatisticsViews.LeftFlowerQuantity import LeftFlowerQuantity
from flowerexchange.InvoiceViews.BenefitViews.NetBenefitOverDay import NetBenefitOverDay
from flowerexchange.InvoiceViews.BenefitViews.NetBenefitByInterval import NetBenefitByInterval


class InvoiceList(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = MyLimitOffsetPagination

    def get(self, request, *args, **kwargs):
        invoices_ = Invoice.objects.filter(is_existed=True)
        pages = self.paginate_queryset(invoices_)
        serializer = InvoiceSerializer(invoices_, many=True)
        results = self.get_paginated_response(serializer.data)

        return Response({'code': 200,
                         'message': 'Success',
                         'data': results.data})


# use custom pagination
class InvoiceListByInterval(generics.RetrieveAPIView):
    serializer_class = InvoiceSerializer
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination

    def get(self, request):
        try:
            start_time = request.data['startTime'] or datetime.date(2020, 1, 1)
            end_time = request.data['endTime'] or datetime.datetime.now()
            invoices_ = Invoice.objects.filter(invoice_time__range=(start_time, end_time))

            page = self.paginate_queryset(invoices_)

            if page is not None:
                serializer = self.get_serializer(page, many=True)
                result = self.get_paginated_response(serializer.data)
                data = result.data  # pagination data
            else:
                serializer = self.get_serializer(page, many=True)
                data = serializer.data

            payload = {
                'code': '200',
                'message': 'Success',
                'data': data
            }

            return Response(payload)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            return Response({'code': 400,
                             'message': 'empty body'}, status=status.HTTP_400_BAD_REQUEST)


# return unpaid customer based on invoices
class UnpaidInvoiceCustomer(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination
    serializer_class = InvoiceSerializer

    def get(self, request, *args, **kwargs):
        try:
            start_time = request.data['startTime'] or datetime.date(2020, 7, 1)
            unpaid_invoices = Invoice.objects.filter(paid=False, invoice_time__gte=start_time).order_by('invoice_time')
            serializer = self.get_serializer(unpaid_invoices, many=True)
            data = serializer.data
            print('data: ', data)
            # check invoice instance and get all unpaid users
            s_list = []
            existed_id = []
            for i in data:
                if i['Users_id_id'] not in existed_id:
                    user = Users.objects.get(id=i['Users_id_id'])
                    s_list.append(user)
                    existed_id.append(i['Users_id_id'])

            pages = self.paginate_queryset(s_list)

            if pages is not None:
                serializer = UserSerializer(pages, many=True)
                result = self.get_paginated_response(serializer.data)
                data = result.data
            else:
                serializer = UserSerializer(pages, many=True)
                data = serializer.data

            payload = {
                'code': 200,
                'message': 'success',
                'results': data
            }

            return Response(payload, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'data not found'}, status=status.HTTP_400_BAD_REQUEST)


class InvoiceNew(generics.CreateAPIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def post(self, request, *args, **kwargs):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token) or None
        try:
            # number
            i_quantity = request.data['quantity']
            i_receive_addr = request.data['receive_address']
            i_exchange_method = request.data['exchangeMethod'] or None

            t_flower_id = request.data['flowerId']
            # flower object
            flower_ = Flowers.objects.get(id=t_flower_id)
            # serialize flower object
            serial_flower = FlowerSerializer(flower_)
            # get flower price
            unit_price = FlowerPrice.objects.get(id=serial_flower.data['FlowerPrice_id'])
            price_ = FlowerPriceSerializer(unit_price)
            # compute cost here

            i_invoice_cost = 0

            if i_exchange_method == 'wholesale':
                i_invoice_cost = price_.data['wholesale_price'] * i_quantity
            elif i_exchange_method == 'sell':
                i_invoice_cost = price_.data['sell_price'] * i_quantity

            # user object
            user_ = Users.objects.get(id=user_id)
            # get the latest stock id
            latest_stock = Stock.objects.all().order_by('-stock_time')[:1]

            # if save stock id that will be easy to make statistics because only access
            t_stock_id = latest_stock[0].id
            # stock object
            stock_ = Stock.objects.get(id=t_stock_id)

            # create new Invoice instance
            invoice_ = Invoice.objects.create(quantity=i_quantity, invoice_cost=i_invoice_cost,
                                              Flowers_id=flower_, Users_id=user_, Stock_id=stock_,
                                              receive_address=i_receive_addr, exchange_method=i_exchange_method)
            serializer = InvoiceSerializer(invoice_)

            # format returned data
            payload = {
                'code': '200',
                'message': 'Success',
                'data': serializer.data
            }
            return Response(payload, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

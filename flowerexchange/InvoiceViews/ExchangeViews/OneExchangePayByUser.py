from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from flowerexchange.paginations import CustomPagination


class OneExchangePayByUser(generics.UpdateAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination

    def patch(self, request):
        try:
            # get invoice id
            invoice_id = request.data['invoiceId']
            invoice = Invoice.objects.get(id=invoice_id)

            serializer = InvoiceSerializer(invoice, data={'paid': True}, partial=True)
            if serializer.is_valid():
                serializer.save()
                payload = {
                    'code': '200',
                    'message': 'Success',
                    'data': serializer.data
                }
                return Response(payload, status=status.HTTP_200_OK)

            return Response({'code': 400,
                             'message': 'cannot update'}, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

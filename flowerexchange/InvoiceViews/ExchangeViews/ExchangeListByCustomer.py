from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist

from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from flowerexchange.paginations import CustomPagination


class ExchangeListByCustomer(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination
    serializer_class = InvoiceSerializer

    def get(self, request, *args, **kwargs):
        try:
            user_id = request.data['userId']
            if user_id:
                invoices_ = Invoice.objects.filter(paid=False, Users_id=user_id).order_by('invoice_time')

                pages = self.paginate_queryset(invoices_)

                if pages is not None:
                    serializer = self.get_serializer(pages, many=True)
                    results = self.get_paginated_response(serializer.data)
                    data = results.data
                else:
                    serializer = self.get_serializer(pages, many=True)
                    data = serializer.data

                payload = {
                    'code': '200',
                    'message': 'Success',
                    'data': data
                }

                return Response(payload, status=status.HTTP_200_OK)

            return Response({'code': 400,
                             'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from flowerexchange.paginations import CustomPagination


class MultipleExchangePayByUser(generics.UpdateAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination

    def patch(self, request, *args, **kwargs):
        try:
            # get invoice id
            invoice_ids = request.data['invoiceId']
            results = []
            for invoice_id in invoice_ids:
                print(invoice_id)
                invoice = Invoice.objects.get(id=invoice_id)

                serializer = InvoiceSerializer(invoice, data={'paid': True}, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    results.append(serializer.data)
            payload = {
                'code': '200',
                'message': 'Success',
                'data': results
            }
            return Response(payload, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

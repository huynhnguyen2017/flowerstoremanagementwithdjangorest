from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from flowermanagement.models import Flowers, FlowerPrice


def common_benefit(start_time):
    invoices_ = Invoice.objects.filter(invoice_time__gte=start_time, is_existed=True, paid=True)

    profit = 0
    for invoice in invoices_:
        current_num = invoice.quantity
        flower = Flowers.objects.get(id=invoice.Flowers_id_id)
        price_ = FlowerPrice.objects.get(id=flower.FlowerPrice_id)
        sell_price = price_.sell_price
        import_price = price_.import_price
        profit += (sell_price - import_price) * current_num
    return profit

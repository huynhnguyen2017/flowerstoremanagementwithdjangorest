from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from django.utils import timezone

from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from .commonBenefit import common_benefit


class NetBenefitOverDay(generics.RetrieveAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request, *args, **kwargs):
        start_time = timezone.now().replace(hour=0, minute=0)

        profit = common_benefit(start_time)

        return Response({'code': 200,
                         'message': 'success',
                         'profit': profit}, status=status.HTTP_200_OK)

from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from .commonBenefit import common_benefit


class NetBenefitByInterval(generics.RetrieveAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request, *args, **kwargs):
        start_time = request.data['startTime']

        profit = common_benefit(start_time)

        return Response({'code': 200,
                         'message': 'success',
                         'profit': profit}, status=status.HTTP_200_OK)

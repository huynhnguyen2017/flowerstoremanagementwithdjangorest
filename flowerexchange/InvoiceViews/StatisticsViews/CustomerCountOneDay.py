from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken


class CustomerCountOneDay(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request, *args, **kwargs):
        try:
            start_time = timezone.now().replace(hour=0, minute=0)
            end_time = timezone.now()
            invoices_ = Invoice.objects.filter(invoice_time__range=(start_time, end_time), paid=True)

            user_total = 0
            users_ = []

            serializer = InvoiceSerializer(invoices_, many=True)

            for value in serializer.data:
                if value['Users_id_id'] not in users_:
                    user_total += 1
                    users_.append(value['Users_id_id'])

            payload = {
                'code': '200',
                'message': 'Success',
                'user_total': user_total
            }

            return Response(payload, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from django.db.models import Count, Sum
import datetime

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from flowerexchange.paginations import CustomPagination
from flowerexchange.serializers import TenMostFlowerSerializer
from stockmanagement.models import Flowers
from flowermanagement.serializers import FlowerSerializer


class TenFlowerMostPurchase(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        try:
            start_time = datetime.datetime.strptime(request.data['startTime'], '%Y-%m-%d')
            end_time = datetime.datetime.now()
            # get all invoices in duration
            invoices = Invoice.objects.filter(invoice_time__range=(start_time, end_time))\
                .values('Flowers_id').annotate(total=Sum('quantity')).order_by('-total')[:10]

            flower_list = []

            for invoice in invoices:
                flower_list.append(Flowers.objects.get(id=invoice['Flowers_id']))

            flower_pages = self.paginate_queryset(flower_list)
            serializer = FlowerSerializer(flower_pages, many=True)

            results = self.get_paginated_response(serializer.data)

            data = results.data
            # can show total, flower name with json
            payload = {
                'code': 200,
                'message': 'Success',
                'data': data
            }

            return Response(payload, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

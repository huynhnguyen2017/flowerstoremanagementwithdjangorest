from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from django.db.models import Sum

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from stockmanagement.models import Stock
from stockmanagement.serializers import StockSerializer


class LeftFlowerQuantity(generics.RetrieveAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request, *args, **kwargs):
        # get date request
        start_time = request.data['startTime']
        # get the latest stock storing
        stock_ = Stock.objects.filter(stock_time__gte=start_time).order_by('-stock_time')[:1]

        benchmark = stock_[0].stock_time

        old_quantity_stock = stock_[0].stock_oldquantity
        new_quantity_stock = stock_[0].stock_newquantity

        # get all invoices from the latest stock storing
        invoice_quantity_total = Invoice.objects.filter(invoice_time__gte=benchmark, paid=True)\
            .annotate(flower_total=Sum('quantity'))
        counter = 0
        # print(invoice_quantity_total)
        for invoice in invoice_quantity_total:
            counter += invoice.flower_total
        r_counter = (old_quantity_stock + new_quantity_stock) - counter
        if r_counter < 0:
            r_counter = 0
        return Response({'code': 200,
                         'message': 'success',
                         'left_quantity': r_counter}, status=status.HTTP_200_OK)



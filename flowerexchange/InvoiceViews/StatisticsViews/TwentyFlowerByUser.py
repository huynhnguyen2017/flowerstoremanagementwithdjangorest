from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from django.core.exceptions import ObjectDoesNotExist
import datetime

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken


class TwentyFlowerByUser(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request, *args, **kwargs):
        try:
            customer_id = request.data['customerId']
            start_time = datetime.datetime.strptime(request.data['startTime'], '%Y-%m-%d')
            end_time = datetime.datetime.now()
            invoices_ = Invoice.objects.filter(Users_id=customer_id,
                                               invoice_time__range=(start_time, end_time), paid=True)\
                .order_by('-invoice_time')[:20]
            invoice_pages = self.paginate_queryset(invoices_)

            serializer = InvoiceSerializer(invoices_, many=True)

            results = self.get_paginated_response(serializer.data)

            payload = {
                'code': '200',
                'message': 'Success',
                'data': results.data
            }

            return Response(payload, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

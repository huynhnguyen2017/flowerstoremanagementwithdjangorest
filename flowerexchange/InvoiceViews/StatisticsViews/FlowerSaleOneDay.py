import datetime
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from flowerexchange.paginations import CustomPagination


class FlowerSaleOneDay(generics.ListAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        try:
            start_time = timezone.now().replace(hour=0, minute=0)
            end_time = timezone.now()
            invoices_ = Invoice.objects.filter(invoice_time__range=(start_time, end_time), paid=True)

            total = 0

            price_total = 0

            serializer = InvoiceSerializer(invoices_, many=True)

            for value in serializer.data:
                total += value['quantity']
                price_total += value['invoice_cost']

            payload = {
                'code': '200',
                'message': 'Success',
                'today_total': total,
                'price_toal': price_total
            }

            return Response(payload, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

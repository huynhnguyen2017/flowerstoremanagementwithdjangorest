from abc import ABC

from rest_framework import serializers

from .models import Invoice


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ['id', 'quantity', 'invoice_cost', 'exchange_method', 'invoice_time',
                  'is_existed', 'Flowers_id_id', 'Users_id_id', 'Stock_id_id', 'paid', 'receive_address']


class TenMostFlowerSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField(read_only=True)

    class Meta:
        model = Invoice
        fields = ['id', 'Flowers_id', 'total']

from django.apps import AppConfig


class FlowermanagementConfig(AppConfig):
    name = 'flowermanagement'

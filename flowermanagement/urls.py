from django.urls import path
from flowermanagement import views


urlpatterns = [
    path('category/one/create/', views.FlowerCategoryNew.as_view(), name='flower-category-one-create'),
    path('category/list/', views.FlowerCategoryList.as_view(), name='flower-category-list'),
    path('one/delete/', views.FlowerDelete.as_view(), name='flower-one-delete'),
    path('list/', views.FlowerList.as_view(), name='flower-list'),
    path('one/create/', views.FlowerNew.as_view(), name='flower-one-create'),
    path('price/one/create/', views.FlowerPriceNew.as_view(), name='flower-price-one-create'),
    path('one/price/delete/', views.FlowerPriceDelete.as_view(), name='flower-one-price-delete'),
    path('price/list/', views.FlowerPriceList.as_view(), name='flower-price-list'),
    path('one/update/', views.OneFlowerPriceUpdate.as_view(), name='flower-one-update'),
]

from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db import IntegrityError
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response

from flowerexchange.models import Invoice
from flowerexchange.serializers import InvoiceSerializer
from stockmanagement.ProjectViews.utils.decodeToken import decode_and_get_user_id
from stockmanagement.permissions import CustomerIsAuthenticatedOrRefreshToken, OwnerIsAuthenticatedOrRefreshToken
from flowermanagement.models import FlowerCategories, Flowers, FlowerPrice
from flowermanagement.serializers import FlowerCategorySerializer, FlowerSerializer, FlowerPriceSerializer


class FlowerCategoryList(APIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            categories = FlowerCategories.objects.all()
            serializer = FlowerCategorySerializer(categories, many=True)

            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerCategoryNew(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:

            try:
                f_category_name = request.data['category_name'] or None

                flower_category = FlowerCategories.objects.create(category_name=f_category_name)
                serializer = FlowerCategorySerializer(flower_category)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except (MultipleObjectsReturned, IntegrityError):
                return Response({'code': 406,
                                 'message': 'flower category existed'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerDelete(generics.DestroyAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def delete(self, request, *args, **kwargs):
        flower_id = request.data['flowerId']
        try:
            flower_ = Flowers.objects.get(id=flower_id)
            serializier = FlowerSerializer(flower_, data={'is_existed': False}, partial=True)

            if serializier.is_valid():
                serializier.save()
                invoices_ = Invoice.objects.filter(Flowers_id=serializier.data['id'])
                for invoice in invoices_:
                    in_serializer = InvoiceSerializer(invoice, data={'is_existed': False}, partial=True)
                    if in_serializer.is_valid():
                        in_serializer.save()

                return Response({'code': 202,
                                 'message': 'success',
                                 'data': serializier.data}, status=status.HTTP_202_ACCEPTED)
            return Response({'code': 400,
                             'message': 'can not delete'}, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'Data not found'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerList(APIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            try:
                flowers = Flowers.objects.filter(is_existed=True)
                serializer = FlowerSerializer(flowers, many=True)
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
            except ObjectDoesNotExist:
                return Response({'code': 404,
                                 'message': 'No data found'}, status=status.HTTP_404_NOT_FOUND)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerNew(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            try:
                f_name = request.data['flowerName'] or None
                f_origin = request.data['origin'] or None
                f_flower_category_id = request.data['flowerCategoryId'] or None
                flower_category = FlowerCategories.objects.get(id=f_flower_category_id)
                f_flower_price_id = request.data['flowerPriceId'] or None
                flower_price = FlowerPrice.objects.get(id=f_flower_price_id)
                flower = Flowers.objects.create(name=f_name, origin=f_origin,
                                                FlowerCategories_id=flower_category, FlowerPrice=flower_price)
                serializer = FlowerSerializer(flower)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except (MultipleObjectsReturned, IntegrityError):
                return Response({'code': 400,
                                 'message': 'flower existed'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerPriceNew(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:

            try:
                im_price = request.data['import_price'] or None
                se_price = request.data['sell_price'] or None
                whole_price = request.data['wholesale'] or None

                flower_price = FlowerPrice.objects.create(import_price=im_price, sell_price=se_price,
                                                          wholesale_price=whole_price)
                serializer = FlowerPriceSerializer(flower_price)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except (MultipleObjectsReturned, IntegrityError):
                return Response({'code': 406,
                                 'message': 'flower price existed'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerPriceDelete(generics.DestroyAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def delete(self, request, *args, **kwargs):
        try:
            flower_price_id = request.data['FlowerPriceId']
            flower_price_ = FlowerPrice.objects.get(id=flower_price_id)
            serializer = FlowerPriceSerializer(flower_price_, data={'is_existed': False}, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'code': 202,
                                 'message': 'success',
                                 'data': serializer.data}, status=status.HTTP_202_ACCEPTED)
            return Response({'code': 400,
                             'message': 'can not delete'}, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'Data not found'}, status=status.HTTP_400_BAD_REQUEST)


class FlowerPriceList(APIView):
    permission_classes = [CustomerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:

            try:
                flower_prices = FlowerPrice.objects.filter(is_existed=True)
                serializer = FlowerPriceSerializer(flower_prices, many=True)
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
            except MultipleObjectsReturned:
                return Response({'code': 400,
                                 'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)
            except ObjectDoesNotExist:
                return Response({'code': 400,
                                 'message': 'data not found'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class OneFlowerPriceUpdate(generics.UpdateAPIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def patch(self, request, *args, **kwargs):
        price_id = request.data['currentFlowerPriceId']
        import_price_ = request.data['importPrice']
        sell_price_ = request.data['sellPrice']
        wholesale_price_ = request.data['wholesalePrice']
        flower_price_ = FlowerPrice.objects.get(id=price_id)

        serializer = FlowerPriceSerializer(flower_price_, data={'import_price': import_price_,
                                                                'sell_price': sell_price_,
                                                                'wholesale_price': wholesale_price_}, partial=True)

        if serializer.is_valid():
            serializer.save()
            payload = {
                'code': 202,
                'message': 'update success',
                'data': serializer.data
            }

            return Response(payload, status=status.HTTP_202_ACCEPTED)
        return Response({'code': 400,
                         'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

from rest_framework import serializers

from flowermanagement.models import FlowerPrice, FlowerCategories, Flowers


class FlowerPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlowerPrice
        fields = ['id', 'sell_price', 'import_price', 'wholesale_price', 'is_existed']


class FlowerCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = FlowerCategories
        fields = ['id', 'category_name', 'is_existed']


class FlowerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flowers
        fields = ['id', 'name', 'origin', 'is_existed', 'use_exp',
                  'FlowerCategories_id_id', 'FlowerPrice_id']
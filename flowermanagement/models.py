from django.db import models


class FlowerPrice(models.Model):
    import_price = models.FloatField(default=0.0)
    sell_price = models.FloatField(default=0.0)
    wholesale_price = models.FloatField(default=0.0)
    is_existed = models.BooleanField(default=True)


class FlowerCategories(models.Model):
    category_name = models.CharField(max_length=100, blank=False, default='')
    is_existed = models.BooleanField(default=True)


# main object
class Flowers(models.Model):
    name = models.CharField(max_length=45, blank=False, default='')
    origin = models.CharField(max_length=100, blank=True, null=True)
    is_existed = models.BooleanField(default=True)
    # used_for = models.CharField(max_length=20, blank=True, default='')
    use_exp = models.DateTimeField(auto_now_add=True)
    FlowerCategories_id = models.ForeignKey(FlowerCategories, on_delete=True, related_name='flowers_categories')
    FlowerPrice = models.ForeignKey(FlowerPrice, on_delete=True, related_name='flowers_price')

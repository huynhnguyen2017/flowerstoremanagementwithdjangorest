from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from rest_framework import status
import datetime
from django.utils.timezone import now

from flowermanagement.models import Flowers, FlowerPrice
from flowermanagement.serializers import FlowerSerializer, FlowerPriceSerializer
from providermanagement.models import Providers
from stockmanagement.permissions import OwnerIsAuthenticatedOrRefreshToken
from stockmanagement.ProjectViews.utils.decodeToken import decode_and_get_user_id
from stockmanagement.models import Import
from importmanagement.serializers import ImportSerializer
from usermanagement.models import Users


class ImportList(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            try:
                imports = Import.objects.all()
                serializer = ImportSerializer(imports, many=True)

                return Response(serializer.data, status=status.HTTP_200_OK)

            except ObjectDoesNotExist:
                return Response({'code': 400,
                                 'message': 'Data not found'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)


class ImportListByInterval(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def get(self, request):
        try:
            start_time = request.data['startTime'] or datetime.date(2020, 1, 1)
            end_time = request.data['endTime'] or now()
            imports_ = Import.objects.filter(import_date__range=(start_time, end_time))

            serializer = ImportSerializer(imports_, many=True)

            return Response(serializer.data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response({'code': 400,
                             'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)


class ImportNew(APIView):
    permission_classes = [OwnerIsAuthenticatedOrRefreshToken]

    def post(self, request):
        api_token = request.META.get('HTTP_AUTHORIZATION')
        user_id = decode_and_get_user_id(api_token)
        if user_id:
            try:
                i_quantity = request.data['quantity'] or None

                i_flowers_id = request.data['flowerId'] or None
                flower_id = Flowers.objects.get(id=i_flowers_id)
                user = Users.objects.get(id=user_id)
                i_providers_id = request.data['providerId'] or None

                provider_id = Providers.objects.get(id=i_providers_id)

                # serialize flower object
                serial_flower = FlowerSerializer(flower_id)
                # get flower price
                unit_price = FlowerPrice.objects.get(id=serial_flower.data['FlowerPrice_id'])
                price_ = FlowerPriceSerializer(unit_price)
                # compute cost here
                i_cost = price_.data['import_price'] * i_quantity or 0

                import_ = Import.objects.create(import_quantity=i_quantity, import_cost=i_cost,
                                                Flowers_id=flower_id, Users_id=user, Providers_id=provider_id)
                serializer = ImportSerializer(import_)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            except (ObjectDoesNotExist, MultipleObjectsReturned):
                return Response({'code': 400,
                                 'message': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'code': 400,
                         'message': 'invalid authentication'}, status=status.HTTP_400_BAD_REQUEST)

from django.urls import path
from importmanagement import views

urlpatterns = [
    path('list/', views.ImportList.as_view(), name='import-list'),
    path('interval/list/', views.ImportListByInterval.as_view(), name='import-interval-list'),
    path('one/create/', views.ImportNew.as_view(), name='import-one-create'),
]

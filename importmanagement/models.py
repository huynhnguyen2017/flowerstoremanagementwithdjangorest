from django.db import models

from flowermanagement.models import Flowers
from providermanagement.models import Providers
from usermanagement.models import Users


class Import(models.Model):
    import_quantity = models.IntegerField(default=0)
    import_cost = models.FloatField(default=0.0)
    import_date = models.DateTimeField(auto_now_add=True)
    is_existed = models.BooleanField(default=True)
    Flowers_id = models.ForeignKey(Flowers, on_delete=True, related_name='import_flowers')
    Users_id = models.ForeignKey(Users, on_delete=True, related_name='import_users')
    Providers_id = models.ForeignKey(Providers, on_delete=True, related_name='import_providers')
from rest_framework import serializers
from .models import Import


# import instance
class ImportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Import
        fields = ['id', 'import_quantity', 'import_cost', 'import_date',
                  'is_existed', 'Flowers_id_id', 'Users_id_id']

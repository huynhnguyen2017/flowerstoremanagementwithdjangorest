from django.apps import AppConfig


class ImportmanagementConfig(AppConfig):
    name = 'importmanagement'
